import java.io.IOException;
import java.util.ArrayList;

public class MatchingMain {

	public static void main(String[] arg) {
		ArrayList<Person> people = babyMaker("Labb1/src/sm-friends.in");
		// l�sa in filer
		// dela upp i listor
		wildOrgy();
		printRes(people);
		// r�kna ut optimal matching
		// skriv ut
	}

	public static ArrayList<Person> babyMaker(String filePath) {
		Reader reader = new Reader();
		ArrayList<String> doc = null;
		try {
			doc = reader.readDoc(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		int couples = 0;
		ArrayList<Person> pepols = new ArrayList<Person>();
		for (String s : doc) {
			if (!s.startsWith("#") && !s.isEmpty()) {
				if (s.startsWith("n=")) {
					couples = Integer.parseInt(s.substring(2));
				} else if (couples != 0) {
					pepols.add(new Person(s));
					couples--;
				} else {
					pepols.get(s.charAt(0)).addPref(s.substring(3));
				}
			}
		}
		return pepols;
	}

	public void wildOrgy(ArrayList<Person> swingers) {
		boolean wed = false;
		while (!wed) {
			wed = allWed(swingers);
			for (int i = 0; i < swingers.size(); i = +2) {
				Person s = swingers.get(i);
				swingers.get(s.getPref()).wed(s);
			}
		}
	}

	public boolean allWed(ArrayList<Person> people) {
		for (Person p : people) {
			if (!p.isWed()) {
				return false;
			}
		}
		return true;
	}

	public void PrintRes(ArrayList<Person> people) {
		String res = new String();
		boolean man = true;
		for (Person p : people) {
			if (man) {
				res = res + p.getName() + " -- " + people.get(p.getWed()).getName() + "\n";
			}
			man = !man;
		}
		try {
			Reader.writeToFile(res, "Labb1/src/sm-friends.out");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}