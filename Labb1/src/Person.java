import java.util.Queue;

public class Person {
	private String name;
	private int nbr;
	private Queue<Integer> interests;
	private int married;
	public Person(String info) {
		String infos[] = info.split(" ");
		nbr = Integer.parseInt(infos[0]);
		name = infos[1];
		married = 0;
	}
	public int getNbr(){
		return nbr;
	}
	public String getName(){
		return name;
	}
	public void addPref(String prefs){
		String infos[] = prefs.split(" ");
		for(String s : infos)
		interests.add(Integer.parseInt(s));
	}
	public boolean wed(Person p){
		if(married > 0){
			return false;
		} else{
			married = p.getNbr();
			p.married = nbr;
			return true;
		}
	}
	public boolean isWed(){
		return married != 0;
	}
	public int getPref(){
		return interests.poll();
	}
	public int getWed(){
		return married;
	}
}
