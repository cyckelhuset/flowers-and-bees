import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Reader {
	public static void writeToFile(String out, String filePath) throws IOException {
		FileWriter writer = new FileWriter(filePath, true);
		writer.write(out);
		writer.close();
	}
	public static ArrayList<String> readDoc(String filePath) throws IOException {
		ArrayList<String> lines = new ArrayList<String>();
		Scanner scan = null;
		scan = new Scanner(filePath);
		while(scan.hasNext()){
			lines.add(scan.nextLine());
		}
		scan.close();
		return lines;
	}
}
